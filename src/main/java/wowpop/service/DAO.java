package wowpop.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import wowpop.model.ServerStats;

/**
 * @author Anton Katernoga
 * @since 30 Dec 2010
 */
public class DAO {
    //    private static final PersistenceManagerFactory PMF = JDOHelper.getPersistenceManagerFactory("keepeye");
    private PersistenceManagerFactory pmf;
    private static final Logger logger = LoggerFactory.getLogger(DAO.class);

    @Transactional
    public void save(Object obj) {
        PersistenceManager pm = pmf.getPersistenceManager();
        pm.makePersistent(obj);
    }

    /**
     * @param range number of elements to return
     * @return list of ServerStats that fit the dates range
     */
    @Transactional
    public List<ServerStats> findInDateRange(long from, long to, long range) {
        List<ServerStats> result;
        PersistenceManager pm = pmf.getPersistenceManager();
        Query q = pm.newQuery(ServerStats.class, "time>=:from && time <=:to");
        q.setOrdering("time ascending");
        q.setRange(0, range);
        result = (List<ServerStats>) q.execute(from, to);
        return result;
    }

    /**
     * @return list of ServerStats that fit the dates range
     */
    @Transactional
    public List<ServerStats> findInDateRange(long from, long to) {
        return findInDateRange(from, to, 350);
    }

    public void deleteAll() {
        PersistenceManager pm = pmf.getPersistenceManager();
        Query q = pm.newQuery(ServerStats.class);
        List<ServerStats> result = (List<ServerStats>) q.execute();
        pm.deletePersistentAll(result);
    }

    public void delete3days() {
        PersistenceManager pm = pmf.getPersistenceManager();
        long to = new Date().getTime();
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(to);
        calendar.add(Calendar.DAY_OF_YEAR, -3);
        long from = calendar.getTimeInMillis();
        Query q = pm.newQuery(ServerStats.class, "time>=:from && time <=:to");
        List<ServerStats> result = (List<ServerStats>) q.execute(from, to);
        pm.deletePersistentAll(result);
    }

    /**
     * @param pmf the pmf to set
     */
    public void setPmf(PersistenceManagerFactory pmf) {
        this.pmf = pmf;
    }

    /**
     * @param hours number of last hours the records are taken for
     * @return list of records for the last N hours
     */
    public List<ServerStats> loadLastRecords(int hours) {
        Calendar startCal = Calendar.getInstance();
        long endTime = startCal.getTimeInMillis();
        Calendar endCal = normalizeTime(startCal);
        endCal.add(Calendar.HOUR, -hours);
        long startTime = endCal.getTimeInMillis();
        List<ServerStats> result = findInDateRange(startTime, endTime);
        return result;
    }

    /**
     * Normalizes time so that the minute is floored to the nearest number, that
     * is divisible by 5 5min is the default interval the cache is valid for
     * 
     * @param cal calendar to normilize
     * @return normalized calendar
     */
    public Calendar normalizeTime(Calendar cal) {
        int mins = cal.get(Calendar.MINUTE);
        cal.set(Calendar.MINUTE, mins - mins % 5);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal;
    }

    /**
     * @param i number of oldest entries to return
     * @return
     */
    public List<ServerStats> findOldest(int i) {
        PersistenceManager pm = pmf.getPersistenceManager();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2000);
        return findInDateRange(calendar.getTimeInMillis(), new Date().getTime(), i);
    }
}
