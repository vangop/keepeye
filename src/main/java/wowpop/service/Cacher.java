package wowpop.service;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import net.sf.jsr107cache.*;

/**
 * @author Anton Katernoga
 * @since 12 Jan 2011
 */
public class Cacher {
    static Cache cache;
    private static final Logger logger = LoggerFactory.getLogger(Cacher.class);
    static {
        try {
            CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
            Map props = createPolicyMap();
            cache = cacheFactory.createCache(props);
        } catch (CacheException e) {
            e.printStackTrace();
        }
    }

    private static Map createPolicyMap() {
        Map props = new HashMap();
        props.put(GCacheFactory.EXPIRATION_DELTA, 300);
        return props;
    }

    public static String fetchCacheStatistics() {
        CacheStatistics stats = cache.getCacheStatistics();
        int hits = stats.getCacheHits();
        int misses = stats.getCacheMisses();
        return "Cache Hits=" + hits + " : Cache Misses=" + misses;
    }

    public static void put(String key, Object value) {
        cache.put(key, value);
    }

    public static Object get(String key) {
        return cache.get(key);
    }

}
