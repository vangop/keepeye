package wowpop.fetchers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import wowpop.model.Realm;
import wowpop.model.ServerStats;

/**
 * @author Anton Katernoga
 * @since 24 Aug 2011
 */
public class MagicMagicFetcher extends AbstractFetcher {

    public MagicMagicFetcher() {
        this.realm = Realm.MAGIC_MAGIC;
    }

    /* (non-Javadoc)
     * @see wowpop.fetchers.AbstractFetcher#parseStatsFromHtml(java.lang.String)
     */
    @Override
    public ServerStats parseStatsFromHtml(String content) {
        Pattern pattern = Pattern.compile("Online players: (\\d+)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
        Matcher matcher = pattern.matcher(content);
        ServerStats res = new ServerStats(realm, 0);
        if (matcher.find()) {
            int pop = Integer.valueOf(matcher.group(1));
            res = new ServerStats(realm, pop);
        }
        return res;
    }

}
