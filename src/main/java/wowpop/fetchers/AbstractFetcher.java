package wowpop.fetchers;

import java.io.IOException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import wowpop.model.Realm;
import wowpop.model.ServerStats;

/**
 * @author Anton Katernoga
 * @since 23 Dec 2010
 */
public abstract class AbstractFetcher implements StatsFetcher {

    protected String serverUrl;
    protected Map<String, String> requestProps;
    protected Realm realm;

    private Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * @return the realm
     */
    public Realm getRealm() {
        return realm;
    }

    /**
     * @param realm the realm to set
     */
    public void setRealm(Realm realm) {
        this.realm = realm;
    }

    /**
     * @param serverUrl the serverUrl to set
     */
    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    /* (non-Javadoc)
     * @see fetchers.StatsFetcher#getStats()
     */
    public ServerStats getStats() {
        String content = null;
        ServerStats result;
        try {
            content = UrlFetcher.getContentFromUrl(serverUrl, requestProps);
            result = parseStatsFromHtml(content);
        } catch (IOException e) {
            logger.error("Was fetching content", e);
            result = new ServerStats(realm, 0);
        }
        return result;
    }

    /**
     * @param content
     * @return stats object filled with the data extracted from the content
     */
    public abstract ServerStats parseStatsFromHtml(String content);

    public void setRequestProps(Map<String, String> requestProps) {
        this.requestProps = requestProps;
    }

}
