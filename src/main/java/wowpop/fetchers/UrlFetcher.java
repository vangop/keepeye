package wowpop.fetchers;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Anton Katernoga
 * @since 23 Dec 2010
 */
public class UrlFetcher {

    private static Logger logger = LoggerFactory.getLogger(UrlFetcher.class);

    public static InputStream getStreamByPost(String url, Properties reqParams) throws IOException {
        URL u = new URL(url);
        HttpURLConnection hcon = (HttpURLConnection) u.openConnection();
        hcon.setRequestMethod("POST");
        hcon.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        hcon.setUseCaches(false);
        hcon.setDoOutput(true);

        DataOutputStream dos = new DataOutputStream(hcon.getOutputStream());
        String encodedParams = encodeParams(reqParams);
        logger.debug("encoded params are {}", encodedParams);
        dos.writeBytes(encodedParams);
        dos.flush();
        dos.close();
        logger.debug("image content type is {}", hcon.getContentType());
        return hcon.getInputStream();
    }

    /**
     * @param reqParams
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String encodeParams(Properties reqParams) throws UnsupportedEncodingException {
        StringBuffer res = new StringBuffer();
        Set<String> keys = reqParams.stringPropertyNames();
        for (Iterator iterator = keys.iterator(); iterator.hasNext();) {
            String key = (String) iterator.next();
            res.append(URLEncoder.encode(key, "UTF-8")).append('=');
            res.append(URLEncoder.encode(reqParams.getProperty(key), "UTF-8"));
            res.append('&');
        }
        res.deleteCharAt(res.length() - 1);
        return res.toString();
    }

    public static String getContentFromUrl(String serverUrl, Map<String, String> conProps) throws IOException {
        BufferedReader reader = null;
        HttpURLConnection connection = null;
        StringBuffer sb = new StringBuffer();
        try {
            URL url = new URL(serverUrl);
            connection = (HttpURLConnection) url.openConnection();
            Set<String> keySet = conProps.keySet();
            for (String key : keySet) {
                connection.addRequestProperty(key, conProps.get(key));
            }
            connection.connect();
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;

            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } finally {
            if (reader != null) {
                reader.close();
            }
            if (connection != null) {
                connection.disconnect();
            }
        }
        return sb.toString();

    }
}
