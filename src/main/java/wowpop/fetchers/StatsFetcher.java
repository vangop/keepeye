package wowpop.fetchers;

import wowpop.model.ServerStats;

/**
 * @author Anton Katernoga
 * @since 23 Dec 2010
 */
public interface StatsFetcher {
    ServerStats getStats();
}
