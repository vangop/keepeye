package wowpop.fetchers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import wowpop.model.Realm;
import wowpop.model.ServerStats;

/**
 * @author Anton Katernoga
 * @since 23 Dec 2010
 */

public class GDFetcher extends AbstractFetcher {

    private Logger logger = LoggerFactory.getLogger(getClass());

    public GDFetcher() {
        realm = Realm.GD_X7;
    }

    /* (non-Javadoc)
     * @see fetchers.AbstractFetcher#parseStatsFromUrl(java.lang.String)
     */
    @Override
    public ServerStats parseStatsFromHtml(String content) {
        Pattern pattern = Pattern.compile("Players online:\\s*</td>\\s*<td>\\s*<b>(\\d+)</b>", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
        Matcher matcher = pattern.matcher(content);
        ServerStats res = new ServerStats(realm, 0);
        if (matcher.find()) {
            int pop = Integer.valueOf(matcher.group(1));
            res = new ServerStats(realm, pop);
        }
        return res;
    }
}
