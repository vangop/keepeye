package wowpop.model;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * @author Anton Katernoga
 * @since 23 Dec 2010
 */
@PersistenceCapable
public class ServerStats {

    @Persistent(primaryKey = "true", valueStrategy = IdGeneratorStrategy.IDENTITY)
    private Long id;
    private int population;
    private long time;
    private Realm realm;

    /**
     * @return the realm
     */
    public Realm getRealm() {
        return realm;
    }

    /**
     * @param realm the realm to set
     */
    public void setRealm(Realm realm) {
        this.realm = realm;
    }

    /**
     * @return the time
     */
    public long getTime() {
        return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(long time) {
        this.time = time;
    }

    /**
     * @param realm TODO
     * @param population - server population
     */
    public ServerStats(Realm realm, int population) {
        this.realm = realm;
        this.population = population;
        this.time = System.currentTimeMillis();
    }

    /**
     * @param population the population to set
     */
    public void setPopulation(int population) {
        this.population = population;
    }

    /**
     * @return the population
     */
    public int getPopulation() {
        return population;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

}
