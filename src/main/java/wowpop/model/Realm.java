package wowpop.model;

/**
 * @author Anton Katernoga
 * @since 4 Jan 2011
 */
public enum Realm {
    GD_X7, ETERNAL_REDEMPTION, MAGIC_MAGIC, MOLTEN_NELTHARION
}
