package wowpop.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import wowpop.fetchers.AbstractFetcher;
import wowpop.fetchers.UrlFetcher;
import wowpop.model.Realm;
import wowpop.model.ServerStats;
import wowpop.service.Cacher;
import wowpop.service.DAO;

/**
 * @author Anton Katernoga
 * @since 21 Dec 2010
 */
@Controller
public class StatsController {
    @Autowired
    private List<AbstractFetcher> fetchers;

    @Autowired
    private DAO dao;

    @Autowired
    @Qualifier("chartConf")
    private Properties chartProps;

    @Value("#{conf['chart.url']}")
    private String chartUrl;

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Value("#{conf['global.show.age']}")
    private int lastDays;

    @RequestMapping(value = "/adm/del")
    public String deleteAll() {
        dao.delete3days();
        return "redirect:/watch.htm";
    }

    @RequestMapping(value = "/adm")
    public ModelAndView admin(HttpServletRequest req) {
        ModelAndView mav = new ModelAndView("adm");
        if (!req.isUserInRole("admin")) {
            logger.debug("User {} detected. Not admin", req.getUserPrincipal());
            mav.setViewName("stats");
        } else {
            List<ServerStats> oldestStat = dao.findOldest(1);
            mav.addObject("oldestItems", oldestStat);
        }
        return mav;
    }

    @RequestMapping(value = "/adm/fd")
    public ModelAndView fetchData() {
        ModelAndView mav = new ModelAndView("fetch");
        List<ServerStats> allStats = new ArrayList<ServerStats>();
        mav.addObject("size", fetchers.size());
        for (int i = 0; i < fetchers.size(); i++) {
            AbstractFetcher fetcher = fetchers.get(i);
            ServerStats stats = fetcher.getStats();
            saveStats(stats);
            allStats.add(stats);
        }
        mav.addObject("stats", allStats);
        return mav;
    }

    /**
     * Filters the input List of records that belong to different realms and
     * forms a Properties object that contains req params for chart api. Chart
     * properties are read from chart.properties, data and legend is formed
     * here.
     * 
     * @param records List of records that need to be filtered.
     * @return Properties with params for chart api
     */
    public Properties formChartRequestParams(List<ServerStats> records) {
        StringBuffer dataBuf = new StringBuffer("t:");
        StringBuffer labelBuf = new StringBuffer();
        for (AbstractFetcher fetcher : fetchers) {
            String filtered = statsByRealmAsString(records, fetcher.getRealm());
            dataBuf.append(filtered).append('|');
            labelBuf.append(fetcher.getRealm()).append('|');
        }
        //removing last |
        dataBuf.deleteCharAt(dataBuf.length() - 1);
        labelBuf.deleteCharAt(labelBuf.length() - 1);
        Properties reqProps = new Properties();
        reqProps.putAll(chartProps);
        reqProps.setProperty("chd", dataBuf.toString());
        reqProps.setProperty("chdl", labelBuf.toString());
        logger.debug("request params are {}", reqProps.toString());
        return reqProps;
    }

    @RequestMapping(value = "/chart.htm")
    public void getChart(HttpServletResponse response) throws IOException {
        StringBuffer keyBuff = new StringBuffer("chart-");
        Calendar c = Calendar.getInstance();
        long to = c.getTimeInMillis();
        c = dao.normalizeTime(c);
        c.add(Calendar.HOUR, -lastDays);
        long from = c.getTimeInMillis();
        keyBuff.append(from);
        String key = keyBuff.toString();
        byte[] image = (byte[]) Cacher.get(key);
        if (image == null) {
            List<ServerStats> records = dao.findInDateRange(from, to);
            Properties reqProps = formChartRequestParams(records);
            InputStream imgStream = UrlFetcher.getStreamByPost(chartUrl, reqProps);
            image = FileCopyUtils.copyToByteArray(imgStream);
            Cacher.put(key, image);
        }
        FileCopyUtils.copy(image, response.getOutputStream());

    }

    /**
     * @param stats
     */
    private void saveStats(ServerStats stats) {
        dao.save(stats);
    }

    /**
     * @param chartProps the chartProps to set
     */
    public void setChartProps(Properties chartProps) {
        this.chartProps = chartProps;
    }

    /**
     * @param chartUrl the chartUrl to set
     */
    public void setChartUrl(String chartUrl) {
        this.chartUrl = chartUrl;
    }

    /**
     * @param dao the dao to set
     */
    public void setDao(DAO dao) {
        this.dao = dao;
    }

    /**
     * @param fetchers the fetchers to set
     */
    public void setFetchers(List<AbstractFetcher> fetchers) {
        this.fetchers = fetchers;
    }

    @RequestMapping(value = "/watch", method = RequestMethod.GET)
    public ModelAndView showStats(@RequestParam(value = "id", required = false) String id, HttpServletRequest req) {
        ModelAndView mav = new ModelAndView("stats");
        if (req.isUserInRole("admin")) {
            logger.debug("Admin user is {} detected", req.getUserPrincipal());
            mav.addObject("admin", true);
        }
        List<ServerStats> result = dao.loadLastRecords(lastDays);
        mav.addObject("stats", result);
        return mav;
    }

    /**
     * @param records List of records to filter
     * @param realm realm to filter by
     * @return List of records from the realm specified in the parameter
     */
    public String statsByRealmAsString(List<ServerStats> records, Realm realm) {
        StringBuffer buf = new StringBuffer();
        for (ServerStats record : records) {
            if (record.getRealm().equals(realm)) {
                buf.append(record.getPopulation()).append(',');
            }
        }
        //remove last ','
        if (buf.length() > 0) {
            buf.deleteCharAt(buf.length() - 1);
        }
        return buf.toString();
    }

}
