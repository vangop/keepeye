<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stats Fetched</title>
</head>
<body>
${size} fetchers processed.
<table border="1">
<thead><tr><td>realm</td><td>population</td></tr></thead>
<c:forEach var="stat" items="${stats }">
    <tr>
        <td>${stat.realm }</td>
        <td>${stat.population }</td>
    </tr>
</c:forEach>
</table>
</body>
</html>