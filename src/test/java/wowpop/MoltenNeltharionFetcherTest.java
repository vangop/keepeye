package wowpop;

import static org.junit.Assert.*;

import org.junit.Test;

import wowpop.fetchers.MoltenNeltharionFetcher;
import wowpop.model.ServerStats;

/**
 * @author Anton Katernoga
 * @since 24 Aug 2011
 */
public class MoltenNeltharionFetcherTest {

    /**
     * Test method for {@link wowpop.fetchers.MoltenNeltharionFetcher#parseStatsFromHtml(java.lang.String)}.
     */
    @Test
    public void testParseStatsFromHtml() {
        String html="<div class=\"mid\">\n" + 
        		"    <div class=\"spacer\"></div>\n" + 
        		"    <div id=\"rs1\" class=\"realmBg neltharionBg\">\n" + 
        		"        <div class=\"players\">\n" + 
        		"            Players: <span>3750</span>\n" + 
        		"        </div>\n" + 
        		"        <div class=\"queue\">\n" + 
        		"            Queue: <span>440</span>\n" + 
        		"        </div>\n" + 
        		"    </div>\n" + 
        		"    <div class=\"spacer\"></div>\n" + 
        		"    <div style=\"display: none;\" id=\"ri1\">\n" + 
        		"        <div class=\"spacer\"></div>\n" + 
        		"        <div\n" + 
        		"            style=\"background: url(/resource/molten/images/panel/factions45x55.png) no-repeat; position: relative;\"\n" + 
        		"            class=\"factionBar\">\n" + 
        		"            <div class=\"ctr\"></div>\n" + 
        		"        </div>\n" + 
        		"        <div class=\"spacer\"></div>\n" + 
        		"        <div class=\"seperator\"></div>\n" + 
        		"        <div class=\"infoR\">\n" + 
        		"            <span class=\"infoL\">Uptime:</span> 2 Days 2 Hours<br><span\n" + 
        		"                class=\"infoL\">Rates:</span> Blizzlike x12\n" + 
        		"        </div>\n" + 
        		"        <div class=\"spacer\"></div>\n" + 
        		"    </div>\n" + 
        		"    <div class=\"seperator\"></div>\n" + 
        		"    <div id=\"rs2\" class=\"realmBg deathwingBg\">\n" + 
        		"        <div class=\"players\">\n" + 
        		"            Players: <span>3750</span>\n" + 
        		"        </div>\n" + 
        		"        <div class=\"queue\">\n" + 
        		"            Queue: <span>391</span>\n" + 
        		"        </div>\n" + 
        		"    </div>\n" + 
        		"    <div class=\"spacer\"></div>\n" + 
        		"    <div style=\"display: none;\" id=\"ri2\">\n" + 
        		"        <div class=\"spacer\"></div>\n" + 
        		"        <div\n" + 
        		"            style=\"background: url(/resource/molten/images/panel/factions40x60.png) no-repeat; position: relative;\"\n" + 
        		"            class=\"factionBar\">\n" + 
        		"            <div class=\"ctr\"></div>\n" + 
        		"        </div>\n" + 
        		"        <div class=\"spacer\"></div>\n" + 
        		"        <div class=\"seperator\"></div>\n" + 
        		"        <div class=\"infoR\">\n" + 
        		"            <span class=\"infoL\">Uptime:</span> 3 Hours 57 Minutes<br><span\n" + 
        		"                class=\"infoL\">Rates:</span> Blizzlike x12\n" + 
        		"        </div>\n" + 
        		"        <div class=\"spacer\"></div>\n" + 
        		"    </div>\n" + 
        		"    <div class=\"seperator\"></div>\n" + 
        		"    <div id=\"rs3\" class=\"realmBg sargerasBg\">\n" + 
        		"        <div class=\"players\">\n" + 
        		"            Players: <span>3753</span>\n" + 
        		"        </div>\n" + 
        		"        <div class=\"queue\">\n" + 
        		"            Queue: <span>580</span>\n" + 
        		"        </div>\n" + 
        		"    </div>\n" + 
        		"    <div class=\"spacer\"></div>\n" + 
        		"    <div style=\"display: none;\" id=\"ri3\">\n" + 
        		"        <div class=\"spacer\"></div>\n" + 
        		"        <div\n" + 
        		"            style=\"background: url(/resource/molten/images/panel/factions45x55.png) no-repeat; position: relative;\"\n" + 
        		"            class=\"factionBar\">\n" + 
        		"            <div class=\"ctr\"></div>\n" + 
        		"        </div>\n" + 
        		"        <div class=\"spacer\"></div>\n" + 
        		"        <div class=\"seperator\"></div>\n" + 
        		"        <div class=\"infoR\">\n" + 
        		"            <span class=\"infoL\">Uptime:</span> 15 Hours 6 Minutes<br><span\n" + 
        		"                class=\"infoL\">Rates:</span> Blizzlike x20\n" + 
        		"        </div>\n" + 
        		"        <div class=\"spacer\"></div>\n" + 
        		"    </div>\n" + 
        		"    <div class=\"seperator\"></div>\n" + 
        		"    <div id=\"rs4\" class=\"realmBg frostwolfBg\">\n" + 
        		"        <div class=\"players\">\n" + 
        		"            Players: <span>3751</span>\n" + 
        		"        </div>\n" + 
        		"        <div class=\"queue\">\n" + 
        		"            Queue: <span>269</span>\n" + 
        		"        </div>\n" + 
        		"    </div>\n" + 
        		"    <div class=\"spacer\"></div>\n" + 
        		"    <div style=\"display: none;\" id=\"ri4\">\n" + 
        		"        <div class=\"spacer\"></div>\n" + 
        		"        <div\n" + 
        		"            style=\"background: url(/resource/molten/images/panel/factions50x50.png) no-repeat; position: relative;\"\n" + 
        		"            class=\"factionBar\">\n" + 
        		"            <div class=\"ctr\"></div>\n" + 
        		"        </div>\n" + 
        		"        <div class=\"spacer\"></div>\n" + 
        		"        <div class=\"seperator\"></div>\n" + 
        		"        <div class=\"infoR\">\n" + 
        		"            <span class=\"infoL\">Uptime:</span> 23 Hours 55 Minutes<br><span\n" + 
        		"                class=\"infoL\">Rates:</span> Blizzlike x3\n" + 
        		"        </div>\n" + 
        		"        <div class=\"spacer\"></div>\n" + 
        		"    </div>\n" + 
        		"    <div class=\"seperator\"></div>\n" + 
        		"    <div id=\"rs5\" class=\"realmBg warsongBg\">\n" + 
        		"        <div class=\"players\">\n" + 
        		"            Players: <span>2654</span>\n" + 
        		"        </div>\n" + 
        		"        <div class=\"queue\">\n" + 
        		"            Queue: <span>0</span>\n" + 
        		"        </div>\n" + 
        		"    </div>\n" + 
        		"    <div class=\"spacer\"></div>\n" + 
        		"    <div style=\"display: none;\" id=\"ri5\">\n" + 
        		"        <div class=\"spacer\"></div>\n" + 
        		"        <div\n" + 
        		"            style=\"background: url(/resource/molten/images/panel/factions55x45.png) no-repeat; position: relative;\"\n" + 
        		"            class=\"factionBar\">\n" + 
        		"            <div class=\"ctr\"></div>\n" + 
        		"        </div>\n" + 
        		"        <div class=\"spacer\"></div>\n" + 
        		"        <div class=\"seperator\"></div>\n" + 
        		"        <div class=\"infoR\">\n" + 
        		"            <span class=\"infoL\">Uptime:</span> 19 Hours 34 Seconds<br><span\n" + 
        		"                class=\"infoL\">Rates:</span> Instant 80 (Pure PvP)\n" + 
        		"        </div>\n" + 
        		"        <div class=\"spacer\"></div>\n" + 
        		"    </div>\n" + 
        		"    <div class=\"seperator\"></div>\n" + 
        		"    <div id=\"rs6\" class=\"realmBg lordaeronBg\">\n" + 
        		"        <div class=\"players\">\n" + 
        		"            Players: <span>3750</span>\n" + 
        		"        </div>\n" + 
        		"        <div class=\"queue\">\n" + 
        		"            Queue: <span>151</span>\n" + 
        		"        </div>\n" + 
        		"    </div>\n" + 
        		"    <div class=\"spacer\"></div>\n" + 
        		"    <div style=\"display: none;\" id=\"ri6\">\n" + 
        		"        <div class=\"spacer\"></div>\n" + 
        		"        <div\n" + 
        		"            style=\"background: url(/resource/molten/images/panel/factions50x50.png) no-repeat; position: relative;\"\n" + 
        		"            class=\"factionBar\">\n" + 
        		"            <div class=\"ctr\"></div>\n" + 
        		"        </div>\n" + 
        		"        <div class=\"spacer\"></div>\n" + 
        		"        <div class=\"seperator\"></div>\n" + 
        		"        <div class=\"infoR\">\n" + 
        		"            <span class=\"infoL\">Uptime:</span> 1 Day 19 Hours<br><span\n" + 
        		"                class=\"infoL\">Rates:</span> Blizzlike x1\n" + 
        		"        </div>\n" + 
        		"        <div class=\"spacer\"></div>\n" + 
        		"    </div>\n" + 
        		"</div>";
        MoltenNeltharionFetcher f=new MoltenNeltharionFetcher();
        ServerStats stats = f.parseStatsFromHtml(html);
        assertEquals(3750, stats.getPopulation());
    }

}
