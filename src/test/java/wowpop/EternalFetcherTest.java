package wowpop;

import junit.framework.Assert;

import org.junit.Test;

import wowpop.fetchers.EternalFetcher;
import wowpop.model.ServerStats;

/**
 * @author Anton Katernoga
 * @since 3 Jan 2011
 */
public class EternalFetcherTest {

    /**
     * Test method for
     * {@link wowpop.fetchers.EternalFetcher#parseStatsFromHtml(java.lang.String)}
     * .
     */
    @Test
    public void testParseStatsFromHtml() {
        String html = "<tr><td align=\"right\">Online Players:</td>"
                + "<td><strong>1349&nbsp;<span style=\"color: rgb(136, 136, 136);\">(</span><span style=\"color: rgb(0, 0, 255);\">562 Alliance</span> <span style=\"color: rgb(136, 136, 136);\">|</span> <span style=\"color: rgb(255, 0, 0);\">787 Horde</span><span style=\"color: rgb(136, 136, 136);\">)</span></strong></td></tr>";
        EternalFetcher fetcher = new EternalFetcher();
        ServerStats res = fetcher.parseStatsFromHtml(html);
        Assert.assertEquals(1349, res.getPopulation());
    }

    /**
     * Test method for
     * {@link wowpop.fetchers.EternalFetcher#parseStatsFromHtml(java.lang.String)}
     * .
     */
    @Test
    public void testParseStatsWhenRegexNotFound() {
        String html = "<option value=\"en\" selected>English</option><option value=\"af\">";
        EternalFetcher fetcher = new EternalFetcher();
        ServerStats res = fetcher.parseStatsFromHtml(html);
        Assert.assertEquals(0, res.getPopulation());
    }

}
