package wowpop;

import static org.junit.Assert.*;

import org.junit.Test;

import wowpop.fetchers.MagicMagicFetcher;
import wowpop.model.ServerStats;

/**
 * @author Anton Katernoga
 * @since 24 Aug 2011
 */
public class MagicMagicFetcherTest {

    @Test
    public void test() {
        String html = "x12</b>\\t\\t\\t\\t\\t\\t\\t<br /><b class=\"onlinePeaked\">&raquo; Online players: 1832 Peaked: 1836</b>\\t\\t\\t\\t\\t\\t</td>\\t\\t\\t\\t\\t\\t<td><img src=\"images/online.png\" title=\"Online\" alt=\"\"></img></td>\\t\\t\\t\\t\\t</tr>\\t\\t\\t\\t</table>\\t\\t\\t\\t<table>    \\t\\t\\t\\t\\t<tr>\\t\\t\\t\\t\\t\\t<td width=\"240\"><b class=\"newsClass\">Molten (Funserver)</b>\\t\\t\\t\\t\\t\\t\\t<br /><b class=\"serverClass\">&raquo; Patch : 3.3.5a 12340</b>\\t\\t\\t\\t\\t\\t\\t<br /><b class=\"serverClass\">&raquo; Drop rate : x40</b>\\t\\t\\t\\t\\t\\t\\t<br /><b class=\"serverClass\">&raquo; Gold rate : x40</b>\\t\\t\\t\\t\\t\\t\\t<br /><b class=\"serverClass\">&raquo; Honor rate : x40</b>\\t\\t\\t\\t\\t\\t\\t<br /><b class=\"serverClass\">&raquo; Experience rate : x40</b>\\t\\t\\t\\t\\t\\t\\t<br /><b class=\"serverClass\">&raquo; Professions rate : x40</b>\\t\\t\\t\\t\\t\\t\\t<br /><b class=\"onlinePeaked\">&raquo; Online players: 1010 Peaked: 1021</b>";
        MagicMagicFetcher f = new MagicMagicFetcher();
        ServerStats statsFromHtml = f.parseStatsFromHtml(html);
        assertEquals(1832, statsFromHtml.getPopulation());
    }

}
