package wowpop;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Properties;

import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import wowpop.fetchers.UrlFetcher;

/**
 * @author Anton Katernoga
 * @since 24 Dec 2010
 */
public class UrlFetcherTest {

    /**
     * Test method for
     * {@link wowpop.fetchers.UrlFetcher#getContentFromUrl(java.lang.String)}.
     * 
     * @throws IOException
     */
    @Test
    public void testGetContentFromRealURL() throws IOException {
        String url = "http://panel.magic-wow.com/web/index.php";
        Resource resource = new ClassPathResource("/config.properties");
        Properties props = PropertiesLoaderUtils.loadProperties(resource);

        HashMap<String, String> map = new HashMap<String, String>();
        map.put("User-Agent", props.getProperty("global.ua"));
        map.put("Cookie", "tos=accepted");
        String contentFromUrl = UrlFetcher.getContentFromUrl(url, map);
        assertFalse(contentFromUrl.isEmpty());
    }

    @Test
    public void testEncoding() throws UnsupportedEncodingException {
        Properties p = new Properties();
        p.setProperty("k2", "v&");
        p.setProperty("k1", "v1");
        String params = UrlFetcher.encodeParams(p);
        assertTrue(params.matches("(k1=v1|k2=v%26)&(k1=v1|k2=v%26)"));
    }

}
