package wowpop;

import static org.junit.Assert.*;

import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;

import wowpop.service.DAO;

/**
 * @author Anton Katernoga
 * @since 12 Jan 2011
 */
public class DAOTest {

    private DAO dao;

    @Before
    public void setUp() {
        dao = new DAO();
    }

    /**
     * Test method for
     * {@link wowpop.service.DAO#normalizeTime(java.util.Calendar)}.
     */
    @Test
    public void testNormalizeTime() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.MINUTE, 13);
        long time1 = dao.normalizeTime(c).getTimeInMillis();
        c.set(Calendar.MINUTE, 10);
        long time2 = dao.normalizeTime(c).getTimeInMillis();
        assertEquals(time1, time2);
    }
}
