package wowpop;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStreamReader;

import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.util.FileCopyUtils;

import wowpop.fetchers.GDFetcher;
import wowpop.model.ServerStats;

/**
 * @author Anton Katernoga
 * @since 24 Dec 2010
 */
public class GDFetcherTest {

    /**
     * Test method for
     * {@link wowpop.fetchers.GDFetcher#parseStatsFromHtml(java.lang.String)}.
     * 
     * @throws IOException
     */
    @Test
    public void testParseStatsFromHardcodedHtml() throws IOException {
        String html = loadFile("mod_realmcore.htm");
        GDFetcher f = new GDFetcher();
        ServerStats stats = f.parseStatsFromHtml(html);
        assertEquals(1959, stats.getPopulation());
    }

    /**
     * @param resource
     * @return
     * @throws IOException
     */
    private String loadFile(String resource) throws IOException {
        Resource r = new ClassPathResource(resource);
        //        InputStream stream = getClass().getClassLoader().getResourceAsStream(resource);
        String res = FileCopyUtils.copyToString(new InputStreamReader(r.getInputStream()));
        return res;
    }
}
