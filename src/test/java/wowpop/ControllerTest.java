package wowpop;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import java.io.IOException;
import java.util.*;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletResponse;

import wowpop.controllers.StatsController;
import wowpop.fetchers.AbstractFetcher;
import wowpop.fetchers.EternalFetcher;
import wowpop.fetchers.GDFetcher;
import wowpop.model.Realm;
import wowpop.model.ServerStats;
import wowpop.service.DAO;

/**
 * @author Anton Katernoga
 * @since 5 Jan 2011
 */
public class ControllerTest {

    private StatsController controller;

    @Before
    public void setUp() {
        controller = new StatsController();
        List<AbstractFetcher> fetchers = new ArrayList<AbstractFetcher>();
        fetchers.add(new GDFetcher());
        fetchers.add(new EternalFetcher());
        controller.setFetchers(fetchers);
        controller.setChartProps(new Properties());
    }

    /**
     * Test method for
     * {@link wowpop.controllers.StatsController#statsByRealmAsString(java.util.List, wowpop.model.Realm)}
     * .
     */
    @Test
    public void testFilterByRealm() {
        List<ServerStats> inputList = new ArrayList<ServerStats>();
        inputList.add(new ServerStats(Realm.ETERNAL_REDEMPTION, 0));
        inputList.add(new ServerStats(Realm.ETERNAL_REDEMPTION, 1));
        inputList.add(new ServerStats(Realm.ETERNAL_REDEMPTION, 2));
        inputList.add(new ServerStats(Realm.GD_X7, 4));
        inputList.add(new ServerStats(Realm.GD_X7, 5));
        String eternals = controller.statsByRealmAsString(inputList, Realm.ETERNAL_REDEMPTION);
        String gds = controller.statsByRealmAsString(inputList, Realm.GD_X7);
        assertTrue("Eternal match", eternals.equals("0,1,2"));
        assertTrue("gds match", gds.equals("4,5"));
    }

    @Test
    public void filterByRealmWithEmptyDataset() {
        List<ServerStats> l = new ArrayList<ServerStats>();
        String eternals = controller.statsByRealmAsString(l, Realm.ETERNAL_REDEMPTION);
    }

    @Test
    public void testFormingOfChartParams() throws IOException {

        List<ServerStats> stats = new ArrayList<ServerStats>();
        stats.add(new ServerStats(Realm.ETERNAL_REDEMPTION, 10));
        stats.add(new ServerStats(Realm.ETERNAL_REDEMPTION, 11));
        stats.add(new ServerStats(Realm.GD_X7, 12));
        stats.add(new ServerStats(Realm.GD_X7, 13));

        Properties params = controller.formChartRequestParams(stats);
        assertEquals("t:12,13|10,11", params.getProperty("chd"));
        assertEquals("GD_X7|ETERNAL_REDEMPTION", params.getProperty("chdl"));
    }

    public void testChartSending() throws IOException {
        DAO dao = mock(DAO.class);
        List<ServerStats> stats = new ArrayList<ServerStats>();
        stats.add(new ServerStats(Realm.ETERNAL_REDEMPTION, 10));
        stats.add(new ServerStats(Realm.GD_X7, 20));
        when(dao.loadLastRecords(anyInt())).thenReturn(stats);
        controller.setDao(dao);
        controller.setChartUrl("http://chart.apis.google.com/chart");
        Properties chartProps = new Properties();
        chartProps.setProperty("chs", "800x250");
        chartProps.setProperty("cht", "lc");
        controller.setChartProps(chartProps);

        //        controller.getChart(new MockHttpServletResponse());
    }
}
